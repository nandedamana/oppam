#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       oppam.py
#       This file is part of OPPAM 0.2.x
#       
#       Copyright (C) 2014, 2018 Nandakumar Edamana <nandakumar@nandakumar.co.in>
#       
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License version 3 as published by
#       the Free Software Foundation.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.
#       
#       
 
import os, subprocess, time
import gi
gi.require_version('Vte', '2.91')
from gi.repository import Vte, Gtk, GLib

PROG_NAME = 'OPPAM'
PROG_VERSION = '0.2.1'
PROG_DESC = 'OPPAM is the acronym for ‘Orca-compatible Python Programming Accessibility Module.’\nIt is a simple IDE for Python developed exclusively for the blind.'

mw = 0	# main window
vb = 0
tv = 0
vt = 0
sw_vt = 0

saved = False
changed = False
tb_change_by_prog = False # used to show whether text buffer is changed by the program or not
filename = "/tmp/untitled.py"
# Three steps are involved: (1) Running python prog, (2) Waiting for keystroke, (3) clear the screen
last_proc = 'none'


#  fork_command_full(pty_flags, working_directory, argv, envv, spawn_flags, child_setup, *child_setup_data)
# fork_command_full() changed to spawn_sync() later.
def vt_fork_cmd(cmd):
	vt.spawn_sync(
                Vte.PtyFlags.DEFAULT, #default is fine
                os.environ['HOME'], #where to start the command?
                cmd, #command or shell -- full path
                [], #it's ok to leave this list empty
                GLib.SpawnFlags.DO_NOT_REAP_CHILD,
                None, #at least None is required
                None,
                )

def add_sw_vt():
	global sw_vt, vt, vb

	sw_vt = Gtk.ScrolledWindow()
	vb.add(sw_vt);
	vt = Vte.Terminal()
	sw_vt.add_with_viewport(vt)
	sw_vt.set_size_request(800, 200)
	
	sw_vt.show()
	vt.show()

def on_tb_changed(widget):
	global changed, tb, tb_change_by_prog, vb
	
	if(not changed and not tb_change_by_prog): changed = True
		
def on_vt_child_exited(widget, status):
	global vt, tv, vb, last_proc

	if(last_proc == 'program'):
		vt_fork_cmd(['/tmp/wait.py'])
		last_proc = 'wait'
	elif(last_proc == 'wait'):
		vt_fork_cmd(['/usr/bin/clear'])
		last_proc = 'clear'
	else:
		vb.remove(sw_vt)
		add_sw_vt()
		tv.grab_focus()
	
def on_btn_run_click(widget):
	global tv, tb, vt, vt_sig, last_proc, filename
	
	if(saved == False):
		filename = "/tmp/untitled.py"
		src = tb.get_text(tb.get_start_iter(), tb.get_end_iter(), True)
		open(filename, "w").write(src)
		
	open("/tmp/wait.py", "w").write("#!/usr/bin/python\n\nimport sys\n\nprint 'Press the Enter Key.'\nsys.stdin.read(1)"); # because the terminal will be clared after each run
	os.system('chmod +x /tmp/wait.py');
	cmd1 = "python "+filename+"\n" # this exit is for child-exited signal that focuses back on TextView

	vt.grab_focus()
	vt_sig = vt.connect("child-exited", on_vt_child_exited)
	last_proc = 'program'
	vt_fork_cmd(['/usr/bin/python', filename])

def on_btn_new_click(widget):
	global changed, saved, tb_change_by_prog
	if(changed):
		dlg = Gtk.MessageDialog(mw, 0, Gtk.MessageType.INFO, Gtk.ButtonsType.YES_NO)
		dlg.set_title("Unsaved Changed!")
		dlg.set_markup("You have not saved the changes that you made. Do you really want to discard?")
		resp = dlg.run()
		if(resp != Gtk.ResponseType.YES):
			return -1;
			
		saved = False
		changed = False
		dlg.destroy()
			
	tb_change_by_prog = True
	tb.set_text('')
	tb_change_by_prog = False
		
def on_btn_open_click(widget):
	global changed, saved, tb_change_by_prog, filename
	
	if(changed):
		dlg = Gtk.MessageDialog(mw, 0, Gtk.MessageType.INFO, Gtk.ButtonsType.YES_NO)
		dlg.set_title("Unsaved Changed!")
		dlg.set_markup("You have not saved the changes that you made. Do you really want to discard?")
		resp = dlg.run()
		if(resp == Gtk.ResponseType.NO):
			return -1;
			
	saved = False
	changed = False
	
	tb_change_by_prog = True
	tb.set_text('')
	tb_change_by_prog = False
			
	dlg = Gtk.FileChooserDialog("Open File", mw, Gtk.FileChooserAction.OPEN, (Gtk.STOCK_OK, Gtk.ResponseType.OK, Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL))
	resp = dlg.run()
	
	if(resp != Gtk.ResponseType.CANCEL and dlg.get_filename() != None):
		filename = dlg.get_filename()
		
		tb_change_by_prog = True
		tb.set_text(open(filename, 'r').read())
		tb_change_by_prog = False
		
		saved = True
	dlg.destroy()
	
def on_btn_save_click(widget):
	global changed, saved, tb_change_by_prog, filename
	
	if(changed):
		if(not saved):
			dlg = Gtk.FileChooserDialog("Save File", mw, Gtk.FileChooserAction.SAVE, (Gtk.STOCK_SAVE, Gtk.ResponseType.OK, Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL))
			resp = dlg.run()
			if(resp != Gtk.ResponseType.CANCEL):
				filename = dlg.get_filename()
				
			dlg.destroy()
	
		open(filename, "w").write(tb.get_text(tb.get_start_iter(), tb.get_end_iter(), True))
		saved = True
		changed = False
		
def on_btn_about_click(widget):
	dlg = Gtk.AboutDialog()
	
	dlg.set_title('About ' + PROG_NAME)
	dlg.set_program_name(PROG_NAME)
	dlg.set_logo_icon_name('oppam')
	dlg.set_version(PROG_VERSION)
	dlg.set_comments(PROG_DESC)
	dlg.set_authors(['Nandakumar Edamana'])
	dlg.set_copyright('Copyright © 2014, 2018 Nandakumar Edamana')
	dlg.set_license('GNU GPL v3')
	dlg.set_website('http://nandakumar.co.in/apps/oppam')
	
	dlg.run()
	dlg.destroy()
	
def draw_main_window():
	global mw, tb, tv, vt, vb, sw_vt
	
	mw = Gtk.Window()
	vb = Gtk.VBox()
	hb = Gtk.HBox()
	
	tbr = Gtk.Toolbar()
	btn_run  = Gtk.Button("_Run")
	btn_run.set_property("use-underline", True)
	btn_new  = Gtk.Button("_New")
	btn_new.set_property("use-underline", True)
	btn_open = Gtk.Button("_Open")
	btn_open.set_property("use-underline", True)
	btn_save = Gtk.Button("_Save")
	btn_save.set_property("use-underline", True)
	btn_about = Gtk.Button("_About")
	btn_about.set_property("use-underline", True)
	icon_run = Gtk.Image()
	
	tv = Gtk.TextView()
	sw = Gtk.ScrolledWindow()
	tb = tv.get_buffer()
	vt = Vte.Terminal()
	sw_vt = Gtk.ScrolledWindow()
	
	sw.add_with_viewport(tv)
	vb.add(sw)
	hb.add(btn_run)
	hb.add(btn_new)
	hb.add(btn_open)
	hb.add(btn_save)
	hb.add(btn_about)
	vb.add(hb)
	sw_vt.add_with_viewport(vt)
	vb.add(sw_vt)
	mw.add(vb)
	
	sw.set_size_request(800, 400)
	sw_vt.set_size_request(800, 200)
	mw.set_title(PROG_NAME)
	
	btn_run.connect("clicked", on_btn_run_click)
	btn_new.connect("clicked", on_btn_new_click)
	btn_open.connect("clicked", on_btn_open_click)
	btn_save.connect("clicked", on_btn_save_click)
	btn_about.connect("clicked", on_btn_about_click)
	tb.connect("changed", on_tb_changed)
	mw.connect("destroy", Gtk.main_quit)
	
	mw.show_all()
	
def main():
	# TODO FIXME rem?
	'''try:
		subprocess.Popen(["orca", "--replace"])
	except:
		print 'Orca seems to be unavailable' '''
	draw_main_window()
	# time.sleep(3)
	mw.present()
	Gtk.main()
	
if __name__ == '__main__':
	main()
